import static org.junit.jupiter.api.Assertions.*;

import main.java.models.Deposit;
import main.java.enums.DepositType;
import main.java.models.User;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

@DisplayName("Test Class for WeDooGift")
class WeDooGiftTest {

    @Test
    @DisplayName("Testing computeExpirationDateFromStartingDate for a GIFT deposit")
    void testComputeExpirationDateFromStartingDate_checkGift_correctDate() {
        Deposit deposit = new Deposit(30, DepositType.GIFT, LocalDate.of(2021, 6, 15).atStartOfDay());
        assertEquals(LocalDate.of(2022, 6, 14), deposit.computeExpirationDateFromStartingDate(deposit.getDepositType()));
    }

    @Test
    @DisplayName("Testing computeExpirationDateFromStartingDate for a MEAL deposit")
    void testComputeExpirationDateFromStartingDate_checkMeal_correctDate() {
        Deposit deposit = new Deposit(30, DepositType.MEAL, LocalDate.of(2020, 1, 1).atStartOfDay());
        assertEquals(LocalDate.of(2021, 2, 28), deposit.computeExpirationDateFromStartingDate(deposit.getDepositType()));
    }

    @Test
    @DisplayName("Testing getGiftExpirationDate for a GIFT deposit")
    void testGetGiftExpirationDate_correctDate() {
        Deposit deposit = new Deposit(30, DepositType.GIFT, LocalDate.of(2021, 6, 15).atStartOfDay());
        assertEquals(LocalDate.of(2022, 6, 14), deposit.getGiftExpirationDate(deposit.getStartingDate()));
    }

    @Test
    @DisplayName("Testing getMealExpirationDate for a MEAL deposit")
    void testGetMealExpirationDate_correctDate() {
        Deposit deposit1 = new Deposit(30, DepositType.MEAL, LocalDate.of(2015, 6, 15).atStartOfDay());
        assertEquals(LocalDate.of(2016, 2, 29), deposit1.getMealExpirationDate(deposit1.getStartingDate()));
        Deposit deposit2 = new Deposit(30, DepositType.MEAL, LocalDate.of(2020, 6, 15).atStartOfDay());
        assertEquals(LocalDate.of(2021, 2, 28), deposit2.getMealExpirationDate(deposit2.getStartingDate()));
    }

    @Test
    @DisplayName("Testing computeBalance")
    void testComputeBalance_rightBalance() {
        User john = new User("John");

        Collection<Deposit> johnDeposits = new ArrayList<>();

        johnDeposits.add(new Deposit(1, DepositType.GIFT, LocalDate.of(2022, 4, 7).atStartOfDay()));

        johnDeposits.add(new Deposit(1, DepositType.MEAL, LocalDateTime.now()));
        johnDeposits.add(new Deposit(1, DepositType.MEAL, LocalDateTime.now()));
        johnDeposits.add(new Deposit(1, DepositType.MEAL, LocalDate.of(2022, 4, 7).atStartOfDay()));

        john.setDeposits(johnDeposits);
        assertEquals(2, john.computeBalance());
    }
}
