package main.java.models;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

/**
 * Class User
 *
 * A User can have multiple deposits (gifts and meals) and has a name
 */
public class User {
    /* CLASS ATTRIBUTES */
    private String name;
    private Collection<Deposit> deposits;

    /* CONSTRUCTORS */
    /**
     * Constructor for class User
     * @param name the name of the user
     */
    public User(String name) {
        this.name = name;
    }

    /* GETTERS AND SETTERS */
    /**
     * Getter for class attribute name
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for class attribute name
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for class attribute deposits
     * @return the deposits
     */
    public Collection<Deposit> getDeposits() {
        return deposits;
    }

    /**
     * Setter for class attribute deposits
     * @param deposits the deposits to set
     */
    public void setDeposits(Collection<Deposit> deposits) {
        this.deposits = deposits;
    }

    /**
     * Computes the total balance the user has in coupons
     * Are only counted the coupons which are still valid today i.e. for which the coupon's start date is
     * before today and the coupon's end date is after today
     *
     * @return the sum of the values of the valid user's coupons
     */
    public double computeBalance() {
        return this.deposits.stream()
            .filter(deposit -> deposit.getStartingDate().isBefore(LocalDateTime.now()) && deposit.getExpirationDate().isAfter(LocalDate.now()))
            .mapToDouble(Deposit::getValue)
            .sum();
    }

}
