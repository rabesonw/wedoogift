package main.java.models;

import main.java.enums.DepositType;
import main.java.helper.DateHelper;

import java.time.*;

/**
 * Class Deposit
 *
 * Represents a "coupon" which has a value, a start date, an expiration date and a type (gift or meal)
 */
public class Deposit {
    /* CLASS ATTRIBUTES */
    private double value;
    private LocalDateTime startingDate;
    private LocalDate expirationDate;
    private DepositType depositType;

    /* CONSTRUCTORS */
    /**
     * Constructor for class Deposit
     * @param value the value of the coupon
     * @param depositType the type of the coupon (gift or meal)
     * @param startingDate the starting date of the coupon validity
     */
    public Deposit(double value, DepositType depositType, LocalDateTime startingDate) {
        this.value = value;
        this.depositType = depositType;
        this.startingDate = startingDate;
        this.expirationDate = computeExpirationDateFromStartingDate(depositType);
    }

    /* GETTERS AND SETTERS */
    /**
     * Getter for class attribute value
     * @return a value
     */
    public double getValue() {
        return value;
    }

    /**
     * Setter for class attribute value
     * @param value the value to set
     */
    public void setValue(double value) {
        this.value = value;
    }

    /**
     * Getter for class attribute startingDate
     * @return the starting date
     */
    public LocalDateTime getStartingDate() {
        return startingDate;
    }

    /**
     * Setter for class attribute startingDate
     * @param startingDate the starting date to set
     */
    public void setStartingDate(LocalDateTime startingDate) {
        this.startingDate = startingDate;
    }

    /**
     * Getter for class attribute expirationDate
     * @return an expiration date
     */
    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    /**
     * Setter for class attribute expirationDate
     * @param expirationDate the date to set
     */
    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     * Getter for class attribute depositType
     * @return an item of the enum DepositType
     */
    public DepositType getDepositType() {
        return depositType;
    }

    /**
     * Setter for class attribute depositType
     * @param depositType the deposit type to set
     */
    public void setDepositType(DepositType depositType) {
        this.depositType = depositType;
    }

    /* CLASS METHODS */
    /**
     * Computes the expiration date of a deposit depending on its type
     * @param depositType the depositType to compute
     * @return the expiration date of the deposit
     */
    public LocalDate computeExpirationDateFromStartingDate(DepositType depositType) {
        return switch (depositType) {
            case GIFT -> getGiftExpirationDate(this.startingDate);
            case MEAL -> getMealExpirationDate(this.startingDate);
        };
    }

    /**
     * Gets the expiration date for a GIFT deposit
     * @return the expiration date of a GIFT deposit
     */
    public LocalDate getGiftExpirationDate(LocalDateTime startingDate) {
        return startingDate.plusDays(364).toLocalDate();
    }

    /**
     * Gets the expiration date for a MEAL deposit
     * @return the expiration date of a MEAL deposit
     */
    public LocalDate getMealExpirationDate(LocalDateTime startingDate) {
        int year = startingDate.plusYears(1).getYear();
        if (DateHelper.isLeapYear(year)) {
            return LocalDate.of(year, 2, 29);
        } else {
            return LocalDate.of(year, 2, 28);
        }
    }
}