package main.java;

import main.java.enums.DepositType;
import main.java.models.Deposit;
import main.java.models.User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Main class WeDooGift
 *
 * Class handling the coupons of users, displays the balance of the users' coupons.
 */
public class WeDooGift {
	public static void main(String[] args) {
		User john = new User("John");

		Collection<Deposit> johnDeposits = new ArrayList<>();

		johnDeposits.add(new Deposit(40.5, DepositType.GIFT, LocalDate.of(2022, 4, 7).atStartOfDay()));
		johnDeposits.add(new Deposit(3, DepositType.GIFT, LocalDate.of(2022, 4, 5).atStartOfDay()));
		johnDeposits.add(new Deposit(10.7, DepositType.GIFT, LocalDate.of(2022, 4, 4).atStartOfDay()));

		johnDeposits.add(new Deposit(13.6, DepositType.MEAL, LocalDateTime.now()));
		johnDeposits.add(new Deposit(12.68, DepositType.MEAL, LocalDateTime.now()));
		johnDeposits.add(new Deposit(1.6, DepositType.MEAL, LocalDate.of(2022, 4, 7).atStartOfDay()));

		john.setDeposits(johnDeposits);

		double johnBalance = john.computeBalance();

		System.out.println(john.getName() +"'s balance is $" + johnBalance);
	}
}
