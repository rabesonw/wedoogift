package main.java.helper;

/**
 * Helper class for dates
 */
public class DateHelper {

    /**
     * Checks if inputted year is leap or not
     * @param year the year to check
     * @return true if leap, otherwise false
     */
    public static boolean isLeapYear(int year) {
        return ((year % 4 == 0) && (year % 100!= 0)) || (year%400 == 0);
    }
}
