package main.java.enums;

/**
 * Enum DepositType
 *
 * Used to store the different types that a "coupon" can take
 */
public enum DepositType {
    GIFT,
    MEAL
}
